package com.yaoa.processor;

import com.yaoa.utils.FieldUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SolePageProcessor implements PageProcessor {

    public static final String URL_LIST = "http://www.51sole.com/company/searchcompany.aspx?q=";

    public static final String URL_DETAIL ="http://www.51sole.com/company/detail_\\d+.html";


    private Site site = Site
            .me()
            .setDomain("http://b2b.huangye88.com")
            .setSleepTime(1000)
            .setRetryTimes(3)
            .setRetryTimes(1000)
            .setUserAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");

    private Map<String,String> mapping = new HashMap<>();

    @Override
    public void process(Page page) {
        if(page.getUrl().get().startsWith(URL_LIST)){
            String currentPage = page.getHtml().xpath("//*[@id=\"pages\"]/u/b/text()").get();
            int currentPageNum = Integer.valueOf(currentPage);

            List<Selectable> linkNodes = page.getHtml().xpath("//*[@id=\"pages\"]/a").nodes();
            Selectable lastLinkNode = linkNodes.get(linkNodes.size() - 1);
            String lastPage = lastLinkNode.xpath("/a/@href").regex("&page=(\\d+)?").get();
            int lastPageNum = Integer.valueOf(lastPage);

            List<String> detailLinks = page.getHtml().xpath("//*[@id=\"main\"]/div/div[@class=\"about_listbox\"]//div[@class=\"t1\"]/p[@class=\"t1_tit\"]/a/@href").all();

            for (String detailLink :detailLinks) {
                if(!mapping.containsKey(detailLink)){
                    mapping.put(detailLink,page.getUrl().get());
                }
            }
            page.addTargetRequests(detailLinks);

            if(currentPageNum < lastPageNum){
                int nextPage = currentPageNum + 1;
                String nextPageUrl = null;
                if(nextPage == 2){
                    nextPageUrl = page.getUrl().get()+"&page="+nextPage;
                }else{
                    nextPageUrl = page.getUrl().get().replaceAll("&page=(\\d+)","&page="+nextPage);
                }
                page.addTargetRequest(nextPageUrl);
            }

        }else{
            page.putField("source","51sole");
            page.putField("targetUrl",page.getUrl().get());
            page.putField("helperUrl",mapping.get(page.getUrl().get()));

            if(page.getUrl().regex("http://www.51sole.com/company/detail_\\d+.html").match()){

                List<Selectable> contactInfoNodes = page.getHtml().xpath("//div[@class=\"profile\"]/div[@class=\"profile-main\"]/div[@class=\"contact-info\"]/ul/li").nodes();
                for (Selectable contactInfoNode : contactInfoNodes) {
                    String itemName = contactInfoNode.xpath("/li/span/em/text()").get().replaceAll(" ","");
                    String fieldName = FieldUtils.getFieldName(itemName);
                    if(fieldName != null){
                        page.putField(fieldName,contactInfoNode.xpath("/li/span/text()").get());
                    }
                }
                String profile = page.getHtml().xpath("//*[@id=\"about\"]/li/div[@class=\"main_body\"]//div[@class=\"article\"]").get();
                page.putField("profile",profile);

                List<Selectable> companyInfoNodes = page.getHtml().xpath("//*[@id=\"about\"]/li/div[@class=\"main_body\"]//div[@class=\"company-info\"]/table/tbody/tr").nodes();
                for (Selectable companyInfoNode : companyInfoNodes) {
                    String itemName = companyInfoNode.xpath("/tr/td[@class=\"c\"]/text()").get();
                    if(itemName != null){
                        String fieldName = FieldUtils.getFieldName(itemName);
                        if(fieldName != null){
                            page.putField(fieldName,companyInfoNode.xpath("/tr/td[2]/text()").get());
                        }
                    }
                }


            }else if(page.getUrl().regex("http://\\w+.51sole.com/").match()){
                List<Selectable> contactNodes = page.getHtml().xpath("//*[@id=\"navcontact\"]/ul/li").nodes();
                for (Selectable contactNode:contactNodes) {
                    String itemName = contactNode.xpath("/li/i/text()").get();
                    if(itemName != null && itemName.length() > 0){
                        String fieldName = FieldUtils.getFieldName(itemName);
                        if(fieldName != null){
                            page.putField(fieldName,contactNode.xpath("/li/span/text()").get());
                        }
                    }
                }

                String profile = page.getHtml().xpath("//*[@id=\"profile\"]/div/div[2]/p/text()").get();
                page.putField("profile",profile);

                List<Selectable> proInfoNodes = page.getHtml().xpath("//*[@id=\"pro_info\"]/div/dl").nodes();
                for (Selectable proInfoNode:proInfoNodes) {
                    String itemName = proInfoNode.xpath("/dl/dt/text()").get();
                    String fieldName = FieldUtils.getFieldName(itemName);
                    page.putField(fieldName,proInfoNode.xpath("/dl/dd/text()").get());
                }
            }
        }
    }

    @Override
    public Site getSite() {
        return site;
    }
}
