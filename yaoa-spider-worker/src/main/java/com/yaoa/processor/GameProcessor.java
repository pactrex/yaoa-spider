package com.yaoa.processor;

import com.yaoa.domain.Game;
import com.yaoa.domain.GameType;
import com.yaoa.utils.JsonUtils;
import us.codecraft.webmagic.*;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Json;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.List;

public class GameProcessor implements PageProcessor {

    private Site site = Site.me();

    @Override
    public void process(Page page) {
        /*List<Selectable> titleNodes = page.getJson().jsonPath("$.*[*].title").nodes();
        List<Selectable> imgNodes = page.getJson().jsonPath("$.*[*].img").nodes();
        int size = titleNodes.size();
        for (int i = 0; i < size ; i++) {
            String title = titleNodes.get(i).get();
            String img = imgNodes.get(i).get();
            page.putField(title,img);
        }*/
        Json callback = page.getJson().removePadding("callgame2");
        //hot
        Selectable hotNode = callback.jsonPath("$.HOT");
        List<Selectable> hotGameNodes = hotNode.nodes();
        List<Game> hotGameList = new ArrayList<>();
        for (Selectable hotGameNode : hotGameNodes) {
            hotGameList.add(JsonUtils.toBean(hotGameNode.get(),Game.class));
        }
        page.putField("HOT",hotGameList);

        //group by first letter
        String str = "ABCDEFGHIGKLMNOPQRSTUVWXYZ";
        for (int i=0;i<str.length();i++){
            String firstLetter = String.valueOf(str.charAt(i));
            Selectable letterNode = callback.jsonPath("$." + firstLetter);
            List<Selectable> letterGameNodes = letterNode.nodes();
            List<Game> gameList = new ArrayList<>();
            for (Selectable letterGameNode : letterGameNodes) {
                gameList.add(JsonUtils.toBean(letterGameNode.get(),Game.class));
            }
            page.putField(firstLetter,gameList);
        }
        //TOP GAME
        Selectable topGameNode = callback.jsonPath("$.TOPGAME");
        List<Selectable> topGameNodes = topGameNode.nodes();
        List<Game> topGameNodeList = new ArrayList<>();
        for (Selectable gameNode : topGameNodes) {
            topGameNodeList.add(JsonUtils.toBean(gameNode.get(),Game.class));
        }
        page.putField("TOPGAME",hotGameList);

        List<Selectable> typeNodes = callback.jsonPath("$.Types").nodes();
        List<GameType> gameTypeList = new ArrayList<>();
        for (Selectable typeNode: typeNodes) {
            gameTypeList.add(JsonUtils.toBean(typeNode.get(),GameType.class));
        }
        page.putField("Types",gameTypeList);
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        /*Spider.create(new GameProcessor()).addUrl("http://sy.5173.com/home/GameSpell?plat=2").addPipeline(new Pipeline() {
            @Override
            public void process(ResultItems resultItems, Task task) {
                System.out.println(JsonUtils.toJSONString(resultItems.getAll()));
            }
        }).run();*/
        Spider.create(new GameProcessor()).addUrl("http://fcd.5173.com/commondata/Category.aspx?type=game1&cache=200&jsoncallback=callgame2").addPipeline(new Pipeline() {
            @Override
            public void process(ResultItems resultItems, Task task) {
                System.out.println(JsonUtils.toJSONString(resultItems.getAll()));
            }
        }).run();
    }
}