package com.yaoa.processor;

import com.yaoa.utils.FieldUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HuangYe88PageProcessor implements PageProcessor {

    public static final String URL_LIST = "http://b2b\\.huangye88\\.com/\\w+/\\w+/(pn\\d+)?";

    public static final String URL_DETAIL = "http://(\\w+.)?b2b\\.huangye88\\.com/(qiye\\d+/|gongsi/\\d+/)?company_detail\\.html";

    private Site site = Site
            .me()
            .setDomain("http://b2b.huangye88.com")
            .setSleepTime(1000)
            .setRetryTimes(3)
            .setRetryTimes(1000)
            .setUserAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");


    private Map<String,String> mapping = new HashMap<>();

    @Override
    public void process(Page page) {
        //列表页
        if (!page.getUrl().regex(URL_DETAIL).match()) {
            List<String> urlDetailList =page.getHtml().xpath("form[@id='jubao']/dl/dt/h4").links().all();
            List<String> newUrlDetailList = new ArrayList<>();
            for (int i=0 ;i < urlDetailList.size(); i++) {
                String urlDetail = urlDetailList.get(i) + "company_detail.html";
                newUrlDetailList.add(urlDetail);
                if(!mapping.containsKey(urlDetail)){
                    mapping.put(urlDetail,page.getUrl().get());
                }
            }
            page.addTargetRequests(newUrlDetailList);

            String result = page.getHtml().xpath("/html/body/div[3]/div[3]/div[1]///div[1]/span/em/text()").get();

            int resultSize = Integer.valueOf(result);
            int pageNum = resultSize % 20 == 0 ? resultSize / 20 : resultSize/20 + 1;

            String currentPage = page.getUrl().regex(URL_LIST).get();
            int currentPageNum = currentPage == null ? 1 : Integer.valueOf(currentPage.substring(2));

            List<String> newUrlList = new ArrayList<>();
            int index =  page.getUrl().get().lastIndexOf("/pn");
            String urlPrefix =  index == -1 ? page.getUrl().get().concat("pn") : page.getUrl().get().substring(0,index).concat("/pn");
            if(currentPageNum < pageNum){
                String nextPage = urlPrefix.concat(++currentPageNum+"");
                newUrlList.add(nextPage);
            }
            page.addTargetRequests(newUrlList);

        }else{
            page.putField("source","黄页88");
            page.putField("helperUrl",mapping.get(page.getUrl().get()));
            page.putField("targetUrl",page.getUrl().get());

            page.putField("manager",page.getHtml().xpath("/html/body/div[3]/div[1]/div[2]/div[2]/ul/li[1]/a/text()").get());
            page.putField("phone",page.getHtml().css("div.telephone").xpath("div/text()").get());
            //公司资料
            page.putField("company", page.getHtml().css("div.r-content").css("p.com-name").xpath("p/text()").get());
            Selectable companyInfo = page.getHtml().css("div.r-content").css("div.data").xpath("ul/li");
            List<Selectable> companyInfoNodes = companyInfo.nodes();
            for (int i=0; i<companyInfoNodes.size(); i++){
                Selectable node = companyInfoNodes.get(i);
                String itemName = node.xpath("//label/text()").get();
                String fieldName = FieldUtils.getFieldName(itemName);
                if(fieldName != null && !fieldName.trim().equals("")){
                    if("category".equals(fieldName)){
                        page.putField(fieldName,node.xpath("li/a/text()").get());
                    }else{
                        page.putField(fieldName,node.xpath("li/text()").get());
                    }
                }
            }
            page.putField("profile", page.getHtml().css("div.r-content").css("p.txt").xpath("p/text()").get());
            //详细资料
            Selectable companyDetailInfo = page.getHtml().css("div.r-content").xpath("table/tbody/tr");
            List<Selectable> companyDetailInfoNodes = companyDetailInfo.nodes();
            for (int i=0; i<companyDetailInfoNodes.size(); i++){
                Selectable node = companyDetailInfoNodes.get(i);
                String itemName = node.xpath("//td/span/text()").get();
                if(itemName != null && !itemName.trim().equals("")){
                    String fieldName = FieldUtils.getFieldName(itemName);
                    if(fieldName != null && !fieldName.trim().equals("")){
                        if("businessScope".equals(fieldName)){
                            page.putField(fieldName,node.xpath("//td/font/text()").get());
                        }else{
                            page.putField(fieldName,node.xpath("//td/text()").get());
                        }
                    }
                }
            }
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public String getFieldName(String chineseName){
        if(chineseName.contains("联系人")){
            return "managerPhone";
        }else if(chineseName.contains("手机")){
            return "managerPhone";
        }else{
            return null;
        }
    }
}
