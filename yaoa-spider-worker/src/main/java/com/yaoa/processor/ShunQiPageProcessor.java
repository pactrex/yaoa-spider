package com.yaoa.processor;

import com.yaoa.utils.FieldUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShunQiPageProcessor implements PageProcessor{

    public static final String URL_LIST = "http://b2b\\.11467\\.com/search/\\d+-?\\d+?\\.htm";

    public static final String URL_DETAIL = "http://www\\.11467\\.com/\\w+/co/\\d+.htm";

    public static final String URL_LIST_PAGE = "http://b2b\\.11467\\.com/search/\\d+-?(\\d+)?\\.htm";

    private Site site = Site
            .me()
            .setDomain("b2b.114067.com")
            .setSleepTime(5000)
            .setRetryTimes(3)
            .setRetrySleepTime(5000)
            .setUserAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");


    private Map<String,String> mapping = new HashMap<>();

    @Override
    public void process(Page page) {
        //列表页
        if (page.getUrl().regex(URL_LIST).match()) {
            List<String> urlDetailList = page.getHtml().xpath("//*[@id=\"il\"]/div[3]/div[1]/div[2]").links().all();
            page.addTargetRequests(urlDetailList);

            for (String urlDetail : urlDetailList) {
                if(!mapping.containsKey(urlDetail)){
                    mapping.put(urlDetail,page.getUrl().get());
                }
            }
            Selectable urlList = page.getHtml().xpath("//*[@id=\"il\"]/div[3]/div[1]/div[1]").links().regex(URL_LIST);
            List<Selectable> pageNodes = urlList.nodes();
            String maxPage = pageNodes.get(pageNodes.size()-1).regex(URL_LIST_PAGE).get();
            int maxPageNum = Integer.valueOf(maxPage);

            String currentPage = page.getUrl().regex(URL_LIST_PAGE).get();
            int currentPageNum = 1;
            if(currentPage != null && !currentPage.trim().equals("")){
                currentPageNum = Integer.valueOf(currentPage);
            }
            List<String> newUrlList = new ArrayList<>();
            if(currentPageNum < maxPageNum){
                int nextPage = currentPageNum + 1;
                String currentUrl = page.getUrl().get();
                if (currentUrl.contains("-")) {
                    newUrlList.add(currentUrl.replaceAll("-\\d+\\.","-"+nextPage+"."));
                } else{
                    newUrlList.add(currentUrl.replaceAll("\\.","-"+nextPage+"."));
                }
                currentPageNum ++;
            }
            page.addTargetRequests(newUrlList);

        } else {   //公司页
            page.putField("source","顺企网");
            page.putField("helperUrl",mapping.get(page.getUrl().get()));
            page.putField("targetUrl",page.getUrl().get());
            page.putField("company", page.getHtml().xpath("//*[@id=\"logo\"]/h1/text()").get());
            page.putField("profile", page.getHtml().xpath("//*[@id=\"aboutus\"]/div").get());

            Selectable link = page.getHtml().xpath("//*[@id=\"contact\"]/div/dl");
            List<Selectable> linkNodes = link.xpath("//dt/text()").nodes();
            int linkNodeSize = linkNodes.size();
            for (int i=0 ;i < linkNodeSize ; i++){
                Selectable node = linkNodes.get(i);
                int index = i+ 1;
                String itemName = node.get();
                if(itemName != null && itemName.trim().length() > 0) {
                    String fieldName = FieldUtils.getFieldName(itemName);
                    if( "managerPhone".equals(fieldName)){
                        page.putField("managerPhoneUrl", link.xpath("//dd["+index+"]/a").regex("css.11467.com/phone/\\d+.jpg").get());
                        page.putField("managerPhoneEncrypt", link.xpath("//dd["+index+"]/a/text()").regex("\\d+\\*{4}").get());
                    }else{
                        page.putField(fieldName,link.xpath("//dd["+index+"]/text()").get());
                    }
                }
            }

            page.putField("address", link.xpath("//dd[1]/text()").get());
            page.putField("landTel", link.xpath("//dd[2]/text()").get());
            page.putField("manager", link.xpath("//dd[3]/text()").get());
            page.putField("managerPhoneUrl", link.xpath("//dd[4]/a").regex("css.11467.com/phone/\\d+.jpg").get());
            page.putField("managerPhoneEncrypt", link.xpath("//dd[4]/a/text()").regex("\\d+\\*{4}").get());
            page.putField("managerEmail", link.xpath("//dd[5]/text()").get());
            page.putField("postCode", link.xpath("//dd[7]/text()").get());
            page.putField("fax", link.xpath("//dd[8]/text()").get());

            Selectable gongshang = page.getHtml().xpath("//*[@id=\"gongshang\"]/div/table/tbody/tr");
            List<Selectable> nodes = gongshang.nodes();
            int size = nodes.size();
            for (int i=0 ;i < size ; i++){
                Selectable node = nodes.get(i);
                String itemName = node.xpath("//td[1]/text()").get();
                if(itemName != null && itemName.trim().length() > 0){

                    String fieldName = FieldUtils.getFieldName(itemName);
                    if(fieldName != null){
                        if( "mainProducts".equals(fieldName) || "category".equals(fieldName) || "officialWebsite".equals(fieldName) || "city".equals(fieldName)){
                            page.putField(fieldName,node.xpath("//td[2]/a/text()").all().toString());
                        }else{
                            page.putField(fieldName,node.xpath("//td[2]/text()").get());
                        }
                    }
                }
            }

            Selectable baiduMapNode = page.getHtml().xpath("//*[@id=\"map\"]/div/script[2]");
            page.putField("mapCompany", baiduMapNode.regex("<h3 style='.*'>(.*)</h3>").get());
            page.putField("mapAddress", baiduMapNode.regex("<p>地址：(.*?)</p>").get());
            page.putField("mapLinkman", baiduMapNode.regex("<p>联系人：(.*?)</p>").get());
            page.putField("mapLinkTel", baiduMapNode.regex("<p>电话：<a\\s+.*?>(.*?)</a></p>").get());
        }
    }



    @Override
    public Site getSite() {
        return this.site;
    }
}
