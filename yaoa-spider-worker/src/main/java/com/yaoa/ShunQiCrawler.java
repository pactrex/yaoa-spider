package com.yaoa;

import com.yaoa.pipeline.CompanyDaoPipeline;
import com.yaoa.processor.HuangYe88PageProcessor;
import com.yaoa.processor.ShunQiPageProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.scheduler.FileCacheQueueScheduler;
import us.codecraft.webmagic.scheduler.QueueScheduler;

/**
 * https://github.com/webmagic-io/jobhunter/blob/master/src/main/java/us/codecraft/jobhunter/JobCrawler.java
 */
@Component
public class ShunQiCrawler {

    @Autowired
    private CompanyDaoPipeline companyDaoPipeline;

    public void crawlShunQi() {
        Spider.create(new ShunQiPageProcessor())
                //从https://github.com/code4craft开始抓
                .addUrl("http://b2b.11467.com/search/11988-46.htm")
                //设置Scheduler，使用Redis来管理URL队列
                //new FileCacheQueueScheduler("D:\\data\\webmagic")
                .setScheduler(new QueueScheduler())
                //设置Pipeline，将结果以json方式保存到文件
//                .addPipeline(new JsonFilePipeline("D:\\data\\webmagic"))
                .addPipeline(companyDaoPipeline)
                //开启5个线程同时执行
                .thread(1)
                //启动爬虫
                .run();
    }

    public static void main(String[] args) {
//        System.out.println("http://b2b.11467.com/search/11987-.htm".matches("http://b2b\\.11467\\.com/search/\\d+-?\\d+?\\.htm"));
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/spring/applicationContext*.xml");
        final ShunQiCrawler companyCrawler = applicationContext.getBean(ShunQiCrawler.class);
        companyCrawler.crawlShunQi();
//        CompanyDaoPipeline pipeLine = applicationContext.getBean(CompanyDaoPipeline.class);
//        Company company = new Company();
//        company.setTargetUrl("http://b2b.11467.com/search/11987-1.htm");
//        pipeLine.testAdd(company);
    }
}
