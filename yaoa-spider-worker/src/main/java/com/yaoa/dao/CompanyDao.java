package com.yaoa.dao;

import com.yaoa.domain.Company;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface CompanyDao {

    @Insert("insert into company (`source`,`helper_url`,`target_url`,`company`,`profile`,`address`,`land_tel`,`manager`,`manager_phone_url`" +
            ",`manager_phone_encrypt`,`manager_email`,`post_code`,`fax`,`legal_person`,`main_products`,`business_scope`,`business_license`," +
            "`issuing_authority`,`issuing_date`,`operation_status`,`operation_mode`,`establish_date`,`employee_count`,`registered_capital`" +
            ",`official_website`,`category`,`province`,`city`,`popularity`,`map_company`,`map_address`,`map_linkman`,`map_link_tel`" +
            ",`company_type`,`main_area`,`main_person_group`,`phone`) values " +
            "(#{source},#{helperUrl},#{targetUrl},#{company},#{profile},#{address},#{landTel},#{manager},#{managerPhoneUrl}" +
            ",#{managerPhoneEncrypt},#{managerEmail},#{postCode},#{fax},#{legalPerson},#{mainProducts},#{businessScope},#{businessLicense}" +
            ",#{issuingAuthority},#{issuingDate},#{operationStatus},#{operationMode},#{establishDate},#{employeeCount},#{registeredCapital}" +
            ",#{officialWebsite},#{category},#{province},#{city},#{popularity},#{mapCompany},#{mapAddress},#{mapLinkman},#{mapLinkTel}" +
            ",#{companyType},#{mainArea},#{mainPersonGroup},#{phone})")
    int add(Company company);

    @Insert("insert into company2 (`source`,`helper_url`,`target_url`,`company`,`profile`,`address`,`land_tel`,`manager`,`manager_phone_url`" +
            ",`manager_phone_encrypt`,`manager_email`,`post_code`,`fax`,`legal_person`,`main_products`,`business_scope`,`business_license`," +
            "`issuing_authority`,`issuing_date`,`operation_status`,`operation_mode`,`establish_date`,`employee_count`,`registered_capital`" +
            ",`official_website`,`category`,`province`,`city`,`popularity`,`map_company`,`map_address`,`map_linkman`,`map_link_tel`" +
            ",`company_type`,`main_area`,`main_person_group`,`phone`) values " +
            "(#{source},#{helperUrl},#{targetUrl},#{company},#{profile},#{address},#{landTel},#{manager},#{managerPhoneUrl}" +
            ",#{managerPhoneEncrypt},#{managerEmail},#{postCode},#{fax},#{legalPerson},#{mainProducts},#{businessScope},#{businessLicense}" +
            ",#{issuingAuthority},#{issuingDate},#{operationStatus},#{operationMode},#{establishDate},#{employeeCount},#{registeredCapital}" +
            ",#{officialWebsite},#{category},#{province},#{city},#{popularity},#{mapCompany},#{mapAddress},#{mapLinkman},#{mapLinkTel}" +
            ",#{companyType},#{mainArea},#{mainPersonGroup},#{phone})")
    int add2(Company company);

    @Insert("insert into company3 (`source`,`helper_url`,`target_url`,`company`,`profile`,`address`,`land_tel`,`manager`,`manager_phone_url`" +
            ",`manager_phone_encrypt`,`manager_email`,`post_code`,`fax`,`legal_person`,`main_products`,`business_scope`,`business_license`," +
            "`issuing_authority`,`issuing_date`,`operation_status`,`operation_mode`,`establish_date`,`employee_count`,`registered_capital`" +
            ",`official_website`,`category`,`province`,`city`,`popularity`,`map_company`,`map_address`,`map_linkman`,`map_link_tel`" +
            ",`company_type`,`main_area`,`main_person_group`,`phone`) values " +
            "(#{source},#{helperUrl},#{targetUrl},#{company},#{profile},#{address},#{landTel},#{manager},#{managerPhoneUrl}" +
            ",#{managerPhoneEncrypt},#{managerEmail},#{postCode},#{fax},#{legalPerson},#{mainProducts},#{businessScope},#{businessLicense}" +
            ",#{issuingAuthority},#{issuingDate},#{operationStatus},#{operationMode},#{establishDate},#{employeeCount},#{registeredCapital}" +
            ",#{officialWebsite},#{category},#{province},#{city},#{popularity},#{mapCompany},#{mapAddress},#{mapLinkman},#{mapLinkTel}" +
            ",#{companyType},#{mainArea},#{mainPersonGroup},#{phone})")
    int add3(Company company);

    @Select("select * from company")
    List<Company> select1();

    @Update("update company set phone = #{phone} where id = #{id}")
    int update(@Param("phone")String phone,@Param("id")Integer id);
}
