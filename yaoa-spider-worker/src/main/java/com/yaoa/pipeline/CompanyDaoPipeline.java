package com.yaoa.pipeline;

import com.alibaba.fastjson.JSON;
import com.yaoa.dao.CompanyDao;
import com.yaoa.domain.Company;
import com.yaoa.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class CompanyDaoPipeline implements Pipeline {

    @Resource
    private CompanyDao companyDao;

    private Logger logger = LoggerFactory.getLogger(CompanyDaoPipeline.class);

    @Override
    public void process(ResultItems resultItems, Task task) {
        Map<String, Object> all = resultItems.getAll();
        if(all.containsKey("targetUrl")){
            String json = JsonUtils.toJSONString(all);
            logger.info(json);
            Company company = JSON.parseObject(json, Company.class);
            if("黄页88".equals(company.getSource())){
                companyDao.add2(company);
            }if("51sole".equals(company.getSource())){
                companyDao.add3(company);
            }else{
                companyDao.add(company);
            }
        }else{
            logger.error(JsonUtils.toJSONString(all));
        }
    }

    public void testAdd(Company company){
        companyDao.add(company);
        logger.info(JsonUtils.toJSONString(company));
    }
}
