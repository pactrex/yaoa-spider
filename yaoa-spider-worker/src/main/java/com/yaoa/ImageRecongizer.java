package com.yaoa;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yaoa.dao.CompanyDao;
import com.yaoa.domain.Company;
import com.yaoa.pipeline.CompanyDaoPipeline;
import com.yaoa.processor.HuangYe88PageProcessor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.scheduler.QueueScheduler;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * https://github.com/webmagic-io/jobhunter/blob/master/src/main/java/us/codecraft/jobhunter/JobCrawler.java
 */
@Component
public class ImageRecongizer {

    @Autowired
    private CompanyDao companyDao;

    public void recongize() {
        List<Company> companies = companyDao.select1();

        Pattern compile = Pattern.compile("css.11467.com/phone/(\\d+){1}.jpg");
        for (Company c : companies) {
            if(c.getManagerPhoneUrl() != null){
                Matcher matcher = compile.matcher(c.getManagerPhoneUrl());
                if(matcher.find()){
                    String str = matcher.group(1);
                    if(str != null){
                        StringBuilder sb = new StringBuilder();
                        int i = 0;
                        while(i <str .length()){
                            if(i % 2 == 1){
                                sb.append(str.charAt(i));
                            }
                            i++;
                        }
                        System.out.println(sb.toString());
                        companyDao.update(sb.toString(),c.getId());
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/spring/applicationContext*.xml");
        final ImageRecongizer imageRecongizer = applicationContext.getBean(ImageRecongizer.class);
        imageRecongizer.recongize();
    }


    public static void test() throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();

        //上传图片
        HttpPost httpPost = new HttpPost("http://ocr.wdku.net/Upload");
        File file = new File("E:\\BaiduNetdiskDownload\\shunqi\\2.jpg");
        HttpEntity entity = MultipartEntityBuilder.create()
                .addPart("id", new StringBody("WU_FILE_27", ContentType.MULTIPART_FORM_DATA))
                .addPart("name", new StringBody("2.jpg", ContentType.MULTIPART_FORM_DATA))
                .addPart("type", new StringBody("image/jpeg", ContentType.MULTIPART_FORM_DATA))
                .addPart("size", new StringBody("176", ContentType.MULTIPART_FORM_DATA))
                .addPart("lastModifiedDate", new StringBody(new Date().toString(), ContentType.MULTIPART_FORM_DATA))
                .addPart("file", new FileBody(file, ContentType.create("image/jpeg"))).build();

        httpPost.setEntity(entity);
        httpPost.addHeader("Cookie","PHPSESSID=6s0rbknq5h7ctvk8k91n4ejvr0");
        HttpResponse httpResponse = httpClient.execute(httpPost);
        HttpEntity httpEntity =  httpResponse.getEntity();
        String uploadResult = EntityUtils.toString(httpEntity);
        System.out.println("上传结果："+uploadResult);

        String uploadId = JSON.parseObject(uploadResult).getJSONObject("data").getString("id");


        //提交识别
        HttpPost ocrPost = new HttpPost("http://ocr.wdku.net/submitOcr?type=1");
        ocrPost.addHeader("Cookie","PHPSESSID=6s0rbknq5h7ctvk8k91n4ejvr0");
        List<NameValuePair> values = new ArrayList<NameValuePair>();
        values.add(new BasicNameValuePair("obj_type","txt"));
        values.add(new BasicNameValuePair("ids",uploadId));
        values.add(new BasicNameValuePair("ts",new Date().getTime()+""));
        values.add(new BasicNameValuePair("lang","3"));
        values.add(new BasicNameValuePair("pass",""));
        ocrPost.setEntity(new UrlEncodedFormEntity(values));
        HttpResponse submitResponse = httpClient.execute(ocrPost);
        String submitResult = EntityUtils.toString(submitResponse.getEntity());
        System.out.println("提交结果："+submitResult);

        JSONObject submitResultJson = JSON.parseObject(submitResult);
        if(!submitResultJson.getInteger("errno").equals(0)){
            return;
        }

        String submitId = submitResultJson.getString("id");


        //轮询识别结果
        boolean pending = true;
        do {
            HttpGet httpGet = new HttpGet("http://ocr.wdku.net/waitResult2?id="+submitId+"&_="+new Date().getTime());
            httpGet.addHeader("Cookie","PHPSESSID=6s0rbknq5h7ctvk8k91n4ejvr0");
            HttpResponse ocrResponse = httpClient.execute(httpGet);
            String ocrResult = EntityUtils.toString(ocrResponse.getEntity());
            System.out.println("识别结果："+ocrResult);
            pending = !JSON.parseObject(ocrResult).getString("desc").equals("Success");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while (pending);

        //下载结果文件
        try {
            DataInputStream dataInputStream = new DataInputStream(new URL("http://ocr.wdku.net/downResult?id=" + submitId + "&t=" + new Date().getTime()).openStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));
            String line = bufferedReader.readLine();
            System.out.println(line);
            bufferedReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
