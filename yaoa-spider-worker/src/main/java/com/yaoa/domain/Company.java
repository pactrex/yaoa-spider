package com.yaoa.domain;

public class Company {

    private Integer id;

    private String source;

    private String helperUrl;

    private String targetUrl;

    private String company;

    private String phone;

    private String profile;

    private String address;

    private String landTel;

    private String manager;

    private String managerPhoneUrl;

    private String managerPhoneEncrypt;

    private String managerEmail;

    private String postCode;

    private String fax;

    private String legalPerson;

    private String mainProducts;

    private String businessScope;

    private String businessLicense;

    private String issuingAuthority;

    private String issuingDate;

    private String operationStatus;

    private String operationMode;

    private String establishDate;

    private String employeeCount;

    private String registeredCapital;

    private String officialWebsite;

    private String category;

    private String province;

    private String city;

    private String popularity;

    private String mapCompany;

    private String mapAddress;

    private String mapLinkman;

    private String mapLinkTel;

    private String companyType;

    private String mainArea;

    private String mainPersonGroup;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getHelperUrl() {
        return helperUrl;
    }

    public void setHelperUrl(String helperUrl) {
        this.helperUrl = helperUrl;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandTel() {
        return landTel;
    }

    public void setLandTel(String landTel) {
        this.landTel = landTel;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getManagerPhoneUrl() {
        return managerPhoneUrl;
    }

    public void setManagerPhoneUrl(String managerPhoneUrl) {
        this.managerPhoneUrl = managerPhoneUrl;
    }

    public String getManagerPhoneEncrypt() {
        return managerPhoneEncrypt;
    }

    public void setManagerPhoneEncrypt(String managerPhoneEncrypt) {
        this.managerPhoneEncrypt = managerPhoneEncrypt;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getMainProducts() {
        return mainProducts;
    }

    public void setMainProducts(String mainProducts) {
        this.mainProducts = mainProducts;
    }

    public String getBusinessScope() {
        return businessScope;
    }

    public void setBusinessScope(String businessScope) {
        this.businessScope = businessScope;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }

    public String getIssuingAuthority() {
        return issuingAuthority;
    }

    public void setIssuingAuthority(String issuingAuthority) {
        this.issuingAuthority = issuingAuthority;
    }

    public String getIssuingDate() {
        return issuingDate;
    }

    public void setIssuingDate(String issuingDate) {
        this.issuingDate = issuingDate;
    }

    public String getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(String operationStatus) {
        this.operationStatus = operationStatus;
    }

    public String getOperationMode() {
        return operationMode;
    }

    public void setOperationMode(String operationMode) {
        this.operationMode = operationMode;
    }

    public String getEstablishDate() {
        return establishDate;
    }

    public void setEstablishDate(String establishDate) {
        this.establishDate = establishDate;
    }

    public String getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(String employeeCount) {
        this.employeeCount = employeeCount;
    }

    public String getRegisteredCapital() {
        return registeredCapital;
    }

    public void setRegisteredCapital(String registeredCapital) {
        this.registeredCapital = registeredCapital;
    }

    public String getOfficialWebsite() {
        return officialWebsite;
    }

    public void setOfficialWebsite(String officialWebsite) {
        this.officialWebsite = officialWebsite;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getMapCompany() {
        return mapCompany;
    }

    public void setMapCompany(String mapCompany) {
        this.mapCompany = mapCompany;
    }

    public String getMapAddress() {
        return mapAddress;
    }

    public void setMapAddress(String mapAddress) {
        this.mapAddress = mapAddress;
    }

    public String getMapLinkman() {
        return mapLinkman;
    }

    public void setMapLinkman(String mapLinkman) {
        this.mapLinkman = mapLinkman;
    }

    public String getMapLinkTel() {
        return mapLinkTel;
    }

    public void setMapLinkTel(String mapLinkTel) {
        this.mapLinkTel = mapLinkTel;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getMainArea() {
        return mainArea;
    }

    public void setMainArea(String mainArea) {
        this.mainArea = mainArea;
    }

    public String getMainPersonGroup() {
        return mainPersonGroup;
    }

    public void setMainPersonGroup(String mainPersonGroup) {
        this.mainPersonGroup = mainPersonGroup;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
