package com.yaoa.downloader;

import java.io.*;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageEncoder;
import com.sun.media.jai.codec.TIFFEncodeParam;
import com.yaoa.utils.JdbcUtil;

import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;

public class ImgDownloader {

    public static void main(String[] args) {
        ImgDownloader downloader = new ImgDownloader();
        ArrayList<String> urlList = downloader.readUrlList();
        downloader.downloadPicture(urlList,"E:\\BaiduNetdiskDownload\\shunqi\\");
    }

    /**
     * 传入要下载的图片的url列表，将url所对应的图片下载到本地
     * @param urlList
     */
    private void downloadPicture(ArrayList<String> urlList,String dir) {
        URL url = null;
        int imageNumber = 0;

        for (String urlString : urlList) {
            try {
                url = new URL("http://"+urlString);
                DataInputStream dataInputStream = new DataInputStream(url.openStream());
                String imageName = dir + imageNumber + ".jpg";
                FileOutputStream fileOutputStream = new FileOutputStream(new File(imageName));

                byte[] buffer = new byte[1024];
                int length;

                while ((length = dataInputStream.read(buffer)) > 0) {
                    fileOutputStream.write(buffer, 0, length);
                }

                dataInputStream.close();
                fileOutputStream.close();

               /* url = new URL("http://"+urlString);
                RenderedOp src = JAI.create("url", url);
                String imageName = dir + imageNumber + ".tiff";
                OutputStream out = new FileOutputStream(imageName);
                TIFFEncodeParam param = new TIFFEncodeParam();
                ImageEncoder enc = ImageCodec.createImageEncoder("TIFF", out, param);
                enc.encode(src);
                out.close();*/

                imageNumber++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 连接mysql数据库，通过查询数据库读取要下载的图片的url列表
     * @return
     */
    private ArrayList<String> readUrlList() {
        ArrayList<String> urlList = new ArrayList<String>();
        try {
            Connection connection = (Connection) JdbcUtil.getConnection();
            Statement statement = (Statement) connection.createStatement();
            String sql = "select manager_phone_url from company"; //查询语句换位相应select语句
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                String url = resultSet.getString("manager_phone_url");
                if(url != null){
                    urlList.add(url);
                    System.out.println(url);
                }
            }

            JdbcUtil.free(resultSet, statement, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return urlList;
    }

    public void convertJpg2tiff(String path) throws IOException {
        RenderedOp src = JAI.create("fileload", path);
        OutputStream out = new FileOutputStream("0.tiff");
        TIFFEncodeParam param = new TIFFEncodeParam();
        ImageEncoder enc = ImageCodec.createImageEncoder("TIFF", out, param);
        enc.encode(src);
        out.close();
    }

}
