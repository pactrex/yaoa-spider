package com.yaoa;

import com.yaoa.pipeline.CompanyDaoPipeline;
import com.yaoa.processor.HuangYe88PageProcessor;
import com.yaoa.processor.SolePageProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.scheduler.QueueScheduler;
@Component
public class Sole51Crawler {
    @Autowired
    private CompanyDaoPipeline companyDaoPipeline;

    public void crawlSole51() {
        Spider.create(new SolePageProcessor())
                //从https://github.com/code4craft开始抓
                .addUrl("http://www.51sole.com/company/searchcompany.aspx?q=贷款")
                //http://senquan22861.b2b.huangye88.com/company_detail.html'
                //设置Scheduler，使用Redis来管理URL队列
                .setScheduler(new QueueScheduler())
                //设置Pipeline，将结果以json方式保存到文件
                .addPipeline(companyDaoPipeline)
                //开启5个线程同时执行
                .thread(5)
                //启动爬虫
                .run();
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/spring/applicationContext*.xml");
        final Sole51Crawler companyCrawler = applicationContext.getBean(Sole51Crawler.class);
        companyCrawler.crawlSole51();
    }
}
