package com.yaoa.utils;

public class FieldUtils {

    public static String getFieldName(String chineseName){
        if(chineseName.contains("公司地址") || chineseName.contains("地址")){
            return "address";
        }else if(chineseName.contains("固定电话") || chineseName.contains("电话")){
            return "landTel";
        }else if(chineseName.contains("经理") || chineseName.contains("联系人")){
            return "manager";
        }/*else if(chineseName.contains("手机号码") || chineseName.contains("公司电话")){
            return "managerPhone";
        }*/else if(chineseName.contains("手机") || chineseName.contains("移动电话") || chineseName.contains("手机号码") || chineseName.contains("公司电话")){
            return "phone";
        }else if(chineseName.contains("邮箱")){
            return "managerEmail";
        }else if(chineseName.contains("邮编")){
            return "postCode";
        }else if(chineseName.contains("传真")){
            return "fax";
        }else if(chineseName.contains("法人名称") || chineseName.contains("企业法人")){
            return "legalPerson";
        }else if(chineseName.contains("主要经营产品") || chineseName.contains("主营产品")){
            return "mainProducts";
        }else if(chineseName.contains("经营范围")){
            return "businessScope";
        }else if(chineseName.contains("营业执照号码")){
            return "businessLicense";
        }else if(chineseName.contains("发证机关")){
            return "issuingAuthority";
        }else if(chineseName.contains("核准日期")){
            return "issuingDate";
        }else if(chineseName.contains("经营状态") || chineseName.contains("企业状态")){
            return "operationStatus";
        }else if(chineseName.contains("经营模式")){
            return "operationMode";
        }else if(chineseName.contains("成立时间")){
            return "establishDate";
        }else if(chineseName.contains("职员人数")){
            return "employeeCount";
        }else if(chineseName.contains("注册资本")){
            return "registeredCapital";
        }else if(chineseName.contains("公司官网")){
            return "officialWebsite";
        }else if(chineseName.contains("所属分类") || chineseName.contains("主营行业")){
            return "category";
        }else if(chineseName.contains("所属城市") || chineseName.contains("所在地")){
            return "city";
        }else if(chineseName.contains("人气值")){
            return "popularity";
        }else if(chineseName.contains("企业类型")){
            return "companyType";
        }else if(chineseName.contains("主营地区")){
            return "mainArea";
        }else if(chineseName.contains("主要客户群")){
            return "mainPersonGroup";
        }else if(chineseName.contains("公司名称")){
            return "company";
        }else{
            return null;
        }
    }
}
