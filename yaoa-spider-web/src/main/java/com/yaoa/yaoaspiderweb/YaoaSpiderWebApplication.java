package com.yaoa.yaoaspiderweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YaoaSpiderWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(YaoaSpiderWebApplication.class, args);
	}
}
